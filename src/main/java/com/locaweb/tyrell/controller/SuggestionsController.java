package com.locaweb.tyrell.controller;

import java.util.List;

import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.locaweb.tyrell.entity.Suggestions;
import com.locaweb.tyrell.repository.ProductRepository;
import com.locaweb.tyrell.repository.SuggestionsRepository;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/api/suggestion")
@Api(value = "Suggestion API")
@CrossOrigin(origins = "*")
public class SuggestionsController {

	@Autowired
	SuggestionsRepository repository;

	@Autowired
	ProductRepository prodRepository;

	@PostMapping(consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Save")
	public ResponseEntity<?> save(@RequestBody Suggestions suggestion) {
		suggestion.setProduct(prodRepository.findById(suggestion.getProduct().getId()).orElse(null));
		return ResponseEntity.ok(repository.save(suggestion));
	}

	@GetMapping(produces = MediaType.APPLICATION_JSON)
	@ApiOperation(value = "All")
	public ResponseEntity<?> all() {
		return ResponseEntity.ok(repository.findAll());
	}

	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON)
	@ApiOperation(value = "FindById")
	public ResponseEntity<?> findById(@PathVariable("id") long id) {
		return ResponseEntity.ok(repository.findById(id));
	}

	@GetMapping(value = "/product/{id}", produces = MediaType.APPLICATION_JSON)
	@ApiOperation(value = "FindByPoductId")
	public ResponseEntity<?> findByProductId(@PathVariable("id") long id) {
		return ResponseEntity.ok(repository.findByProductId(id));
	}

	@PutMapping(consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Update")
	public ResponseEntity<?> update(@RequestBody Suggestions suggestion) {
		Suggestions sug = repository.findById(suggestion.getId()).orElse(null);
		if (sug == null) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(repository.save(sug));
	}

	@DeleteMapping(value = "/{id}")
	@ApiOperation(value = "Delete")
	public ResponseEntity<?> delete(@PathVariable("id") long id) {
		Suggestions sug = repository.findById(id).orElse(null);
		if (sug == null) {
			return ResponseEntity.notFound().build();
		}
		repository.delete(sug);
		return ResponseEntity.ok("Deleted");
	}

	@GetMapping(value = "/product/{id}/rate", produces = MediaType.APPLICATION_JSON)
	@ApiOperation(value = "FindByProductRateMedia")
	public ResponseEntity<?> mediaRate(@PathVariable("id") long id) {
		List<Suggestions> list = repository.findByProductId(id);
		double aux = 0;
		int count = 0;
		for (Suggestions suggestions : list) {
			aux = suggestions.getRate() + aux;
			count++;
		}
		double a = aux / count;
		return ResponseEntity.ok(a);
	}

	@GetMapping(value = "/product/{id}/price", produces = MediaType.APPLICATION_JSON)
	@ApiOperation(value = "FindByProductPriceMedia")
	public ResponseEntity<?> mediaPrice(@PathVariable("id") long id) {
		List<Suggestions> list = repository.findByProductId(id);
		double aux = 0;
		int count = 0;
		for (Suggestions suggestions : list) {
			aux = suggestions.getPrice() + aux;
			count++;
		}
		double a = aux / count;
		return ResponseEntity.ok(a);
	}
}
