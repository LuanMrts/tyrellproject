package com.locaweb.tyrell.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.locaweb.tyrell.entity.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

	List<Product> findByEanIgnoreCaseContaining(String id);

	List<Product> findByTitleIgnoreCaseContaining(String id);

}
