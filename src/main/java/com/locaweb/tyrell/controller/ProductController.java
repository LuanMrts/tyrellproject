package com.locaweb.tyrell.controller;

import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.locaweb.tyrell.entity.Product;
import com.locaweb.tyrell.repository.ProductRepository;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/api/product")
@Api(value = "Product API")
@CrossOrigin(origins = "*")
public class ProductController {

	@Autowired
	ProductRepository repository;

	@PostMapping(consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Save")
	public ResponseEntity<?> save(@RequestBody Product product) {
		return ResponseEntity.ok(repository.save(product));
	}

	@GetMapping(produces = MediaType.APPLICATION_JSON)
	@ApiOperation(value = "All")
	public ResponseEntity<?> all() {
		return ResponseEntity.ok(repository.findAll());
	}

	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON)
	@ApiOperation(value = "FindById")
	public ResponseEntity<?> findById(@PathVariable("id") long id) {
		return ResponseEntity.ok(repository.findById(id));
	}

	@PutMapping(consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Update")
	public ResponseEntity<?> update(@RequestBody Product product) {
		Product prod = repository.findById(product.getId()).orElse(null);
		if (prod == null) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(repository.save(product));
	}

	@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Delete")
	public ResponseEntity<?> delete(@PathVariable("id") long id) {
		Product prod = repository.findById(id).orElse(null);
		if (prod == null) {
			return ResponseEntity.notFound().build();
		}
		repository.delete(prod);
		return ResponseEntity.ok("Deleted");
	}

	@GetMapping(value = "/ean/{ea}", produces = MediaType.APPLICATION_JSON)
	@ApiOperation(value = "FindByEan")
	public ResponseEntity<?> findByEan(@PathVariable("ea") String id) {
		return ResponseEntity.ok(repository.findByEanIgnoreCaseContaining(id));
	}

	@GetMapping(value = "/name/{nam}", produces = MediaType.APPLICATION_JSON)
	@ApiOperation(value = "FindbyName")
	public ResponseEntity<?> findByName(@PathVariable("nam") String id) {
		return ResponseEntity.ok(repository.findByTitleIgnoreCaseContaining(id));
	}
}
