package com.locaweb.tyrell.entity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "T_SUGGESTIONS")
@SequenceGenerator(name = "seqSug", sequenceName = "SEQ_SUGGESTIONS")
public class Suggestions {

	@Id
	@GeneratedValue(generator = "seqSug", strategy = GenerationType.SEQUENCE)
	private long id;

	@Column
	private String description;

	@Column
	private String store;

	@Column
	private String pros;

	@Column
	private String contras;

	@Column
	private String performance;

	@Column
	private double rate;

	@Column
	private String customerName;

	@Column
	private double price;
	
	@Column
	private LocalDate createdAt;

	@ManyToOne
	@JoinColumn(name = "product_id")
	private Product product;

	@PrePersist
	public void onCreate() {
		if (createdAt == null)
			createdAt = LocalDate.now();
	}

}
