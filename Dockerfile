FROM docker-registry.locaweb.com.br/jessie/java-dev:8

RUN mkdir -p /opt/tyrellApi/debian
ADD . /opt/tyrellApi
VOLUME /opt/tyrellApi
WORKDIR /opt/tyrellApi

RUN ["./gradlew", "--no-daemon", "resolveDependencies"]
