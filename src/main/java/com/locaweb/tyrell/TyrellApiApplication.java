package com.locaweb.tyrell;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TyrellApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(TyrellApiApplication.class, args);
	}

}
