![](logo.jpg)
# [MillY-API] - [Tyrell]

## Integrantes
- [Michel Lima] (Dev)
- [Luan Mendes] (Dev)
- [Amanda Oliveira] (Business)
- [Michele] (Business)

## Descrição
API, utilizada para fornecer os produtos através do nome e/ou Código de Barras (EAN), e sugestão do produtos desejados
