package com.locaweb.tyrell.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "T_PRODUCTS")
@SequenceGenerator(name = "seqProd", sequenceName = "SEQ_PRODUCT")
public class Product {

	@Id
	@GeneratedValue(generator = "seqProd", strategy = GenerationType.SEQUENCE)
	private long id;

	@Column
	private String title;

	@Column
	private String description;

	@Column
	private double price;

	@Column
	private String ean;

	@Column
	private String image;
}
