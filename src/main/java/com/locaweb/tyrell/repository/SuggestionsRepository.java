package com.locaweb.tyrell.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.locaweb.tyrell.entity.Suggestions;

@Repository
public interface SuggestionsRepository extends JpaRepository<Suggestions, Long> {

	List<Suggestions> findByProductId(long id);
}
